#!/bin/bash

make x86_64_defconfig
if [ "$?" != "0" ]
then
	echo "Failed to select x86_64_defconfig"
    exit 1
fi

cat << EOF > .config
CONFIG_STAGING=y
CONFIG_GPIOLIB=y

CONFIG_VME_BUS=y
CONFIG_VME_CA91CX42=y
CONFIG_VME_TSI148=y
CONFIG_VME_FAKE=y
CONFIG_VMIVME_7805=y
CONFIG_VME_USER=y
CONFIG_VME_PIO2=y
EOF

yes "" | make oldconfig
if [ "$?" != "0" ]
then
	echo "Failed to update config"
    exit 1
fi

make
if [ "$?" != "0" ]
then
	echo "Failed to build kernel"
    exit 1
fi

exit 0

#!/bin/bash

CWD=$(pwd)
if [ "$?" != "0" ]
then
	echo "Failed to determine working directory" >&2
	exit 1
fi

cd poky
if [ "$?" != "0" ]
then
	echo "Failed to enter poky directory" >&2
	exit 1
fi

source oe-init-build-env ../build
if [ "$?" != "0" ]
then
	echo "Failed to source poky init script" >&2
	exit 1
fi

LAYERS=("meta-openembedded/meta-oe" "meta-vmeutils")

for LAYER in ${LAYERS[@]}
do
	grep "${LAYER}" conf/bblayers.conf 1>/dev/null
	if [ "$?" != "0" ]
	then
		echo "BBLAYERS += \" ${CWD}/${LAYER}\"" >> conf/bblayers.conf
		if [ "$?" != "0" ]
		then
			echo "Failed to add ${LAYER} to bblayers.conf" >&2
			exit 1
		fi
	fi
done

grep "INHERIT += \"rm_work\"" conf/local.conf 1>/dev/null
if [ "$?" != "0" ]
then
	echo "INHERIT += \"rm_work\"" >> conf/local.conf
	if [ "$?" != "0" ]
	then
		echo "Failed to add rm_work to local.conf" >&2
		exit 1
	fi
fi

grep "PREFERRED_PROVIDER_virtual/kernel" conf/local.conf 1>/dev/null
if [ "$?" != "0" ]
then
	echo "PREFERRED_PROVIDER_virtual/kernel = \"linux-yocto-vme\"" >> conf/local.conf
	if [ "$?" != "0" ]
	then
		echo "Failed to add preferred kernel to local.conf" >&2
		exit 1
	fi
fi

export MACHINE="qemux86-64"
export LC_ALL=en_US.UTF-8
export LANG=en_US.UTF-8
export LANGUAGE=en_US.UTF-8

bitbake vme-image-dev
if [ "$?" != "0" ]
then
	echo "Failed to build vme-image-dev" >&2
	exit 1
fi

FROM debian:jessie
MAINTAINER Martyn Welch <martyn.welch@collabora.co.uk>
RUN apt-get update -qq
RUN apt-get install -y -qq gcc make bc libelf-dev bison flex

